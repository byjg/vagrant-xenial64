#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

sudo sed -ie 's/localhost/localhost ubuntu-xenial/g' /etc/hosts

sudo apt-get install -q -f -y aptitude

sudo aptitude update -q

# Fix Locale
sudo aptitude install -q -f -y language-pack-pt
sudo locale-gen en_US.UTF-8 pt_BR.UTF8
#sudo dpkg-reconfigure locales
sudo touch /etc/profile.d/10-environment.sh
sudo sh -c "cat >> /etc/profile.d/10-environment.sh << 'EOF'
export LC_ALL=pt_BR.UTF-8
export LANG=pt_BR.UTF-8
EOF"
sudo chmod a+x /etc/profile.d/10-environment.sh
sudo /etc/profile.d/10-environment.sh

# Force a blank root password for mysql
echo "mysql-server mysql-server/root_password password vagrant" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password vagrant" | sudo debconf-set-selections

# Install mysql, nginx, php5-fpm
sudo aptitude install -q -y -f mysql-server mysql-client nginx php-fpm vim-nox

# Install commonly used php packages
sudo aptitude install -q -y -f php-mysql php-curl php-gd php-pear php-imagick php-imap php-mcrypt php-memcached php-sqlite3 php-tidy php-xml php-xdebug

sudo rm /etc/nginx/sites-available/default
sudo touch /etc/nginx/sites-available/default
sudo sh -c "sudo cat >> /etc/nginx/sites-available/default <<'EOF'
## 
# Default server configuration
#
server {
  listen 80 default_server;
  listen [::]:80 default_server;

  index index.html index.htm index.nginx-debian.html index.php;

  server_name _;

  set \$domain \$host;

  # Remove 'www'
  if (\$domain ~ \"^(w{3}\.)(.*)\") {
    set \$domain \$2;
  }
  
  set \$base /vagrant/\$domain;

  # If does not exists, uses 127.0.0.1
  if ( !-d \$base ) {
    set \$domain "127.0.0.1";
    set \$base /vagrant/\$domain;
  }

  # Check for common public folders.
  if ( -d \$base/web ) {
    set \$base \$domain/web;
  }
  if ( -d \$base/httpdocs ) {
    set \$base \$domain/httpdocs;
  }
  if ( -d \$base/public ) {
    set \$base \$domain/public;
  }
  
  
  access_log /var/log/nginx/\$domain.access.log;
  error_log /var/log/nginx/error.log;

  root \$base/;


  location / {
    # First attempt to serve request as file, then
    # as directory, then fall back to displaying a 404.
	#try_files \$uri \$uri/ /index.php /route.php /app.php =404;
  }

  # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
  #
  location ~ \.php$ {
    include snippets/fastcgi-php.conf;
	
    # With php7.0-cgi alone:
    #fastcgi_pass 127.0.0.1:9000;
    # With php7.0-fpm:
    fastcgi_pass unix:/run/php/php7.0-fpm.sock;
  }

  # deny access to .htaccess files, if Apache's document root
  # concurs with nginx's one
  #
  location ~ /\.ht {
    deny all;
  }
}
EOF"

sudo mkdir -p /vagrant/127.0.0.1
sudo rm /vagrant/127.0.0.1/index.html
sudo touch /vagrant/127.0.0.1/index.html
sudo sh -c "cat >> /vagrant/127.0.0.1/index.html <<'EOF'
<h1>Your domain was not setup</h1>

Please read the README.md
EOF"

sudo mkdir -p /vagrant/10.10.100.110
sudo rm /vagrant/10.10.100.110/info.php
sudo touch /vagrant/10.10.100.110/info.php
sudo sh -c "cat >> /vagrant/10.10.100.110/info.php <<'EOF'
<?php phpinfo(); ?>
EOF"


sudo service nginx restart

sudo service php7.0-fpm restart

