# Vagrant Ubuntu Xenial64 (16.04)

This installation provides:

* PHP 7.0
* MySQL 5.7
* NGinx zero-conf multiple domains + PHP5-FPM
* Apache (optional)
* Shared Folder: ./code
* Host IP: 10.10.100.110
* MySQL Username: root and password: vagrant

## installation

### First time

* Install the latest Virtualbox
* Install the latest Vagrant
* Install the vagrant-vbguest (see below)

```
vagrant plugin install vagrant-vbguest
```

### Running

```
vagrant up 
```

### Issues

#### Box creation is aborted.

The image host does not setup the hostname and Ubuntu 16.04 now returns error when the hostname does not exists. 
Because that the operation fail and the provision does not run. 

As workaround edit the file `configure_networks` and add "|| true" at the end of lines 45, 46, 54

```
sudo vim /opt/vagrant/embedded/gems/gems/vagrant-1.8.1/plugins/guests/debian/cap/configure_networks.rb
```

#### IP 10.10.100.110 is not set

This is a bug on xenial64 and vagrant. This article explain how to fix: 
http://able.cd/b/2012/04/09/vagrant-broken-networking-when-packaging-ubuntu-boxes/

But I did not can fix it. 

As Workaraound access the machine using the port 8081 for Nginx and 33307 for MySQL

## Note about zero-conf multiple domains

This install enables you to run multiple domain with zero-conf in the Nginx.

### How it works?

Just create a symbolic link with the domain name inside the `<shared folder>`. Like this:

```
ln -s /path/to/my/project ./code/domain.name.com 
sudo sh -c 'echo "10.10.100.110    domain.name.com      # For Vagrant" >> /etc/hosts'
```

All inside that folder will be available to the nginx. 

If inside the folder `./code/domain.name.com` has the folder `public`, `web` or `httpdocs` the Nginx will use it as root folder. 

